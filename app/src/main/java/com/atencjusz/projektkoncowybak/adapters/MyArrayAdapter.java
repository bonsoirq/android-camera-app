package com.atencjusz.projektkoncowybak.adapters;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.atencjusz.projektkoncowybak.R;
import com.atencjusz.projektkoncowybak.activities.ChooseAlbumActivity;

import java.io.File;

/**
 * Created by 4id2 on 2016-09-22.
 */
public class MyArrayAdapter extends ArrayAdapter {
    public MyArrayAdapter(Context context, int resource, String[] objects) {
        super(context, resource, objects);
        array = objects;
        _context = context;
    }
    private String[] array;
    private Context _context;

    public MyArrayAdapter(Context context, int resource, int textViewResourceId, String[] objects) {
        super(context, resource, textViewResourceId, objects);
        array = objects;
        _context = context;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        //return super.getView(position, convertView, parent);
        //inflater - klasa konwertująca xml na kod javy
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(
                R.layout.layout_choose_album,
                null);
        //szukam kontrolki w layoucie

        TextView tv1 = (TextView) convertView.findViewById(R.id.textView_layout_choose_album);
        tv1.setText(array[position]);
        //
        ImageView iv1 = (ImageView) convertView.findViewById(R.id.imageView_layout_choose_album);
        iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(_context, array[position], Toast.LENGTH_SHORT).show();
                File toDelete = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES),
                        "JaroslawBak");
                //toDelete = new File(toDelete, "JaroslawBak");
                toDelete = new File(toDelete, array[position]);
                Log.i("file", toDelete.getAbsolutePath());

                //ArrayList<String> directoryContents = new ArrayList<String>();
                for (File f : toDelete.listFiles()){
                    f.delete();
                }
                toDelete.delete();
                //Intent intent = new Intent(_activity, ChooseAlbumActivity.class);
                ((ChooseAlbumActivity)_context).restart();

            }
        });

        return convertView;

    }
}
