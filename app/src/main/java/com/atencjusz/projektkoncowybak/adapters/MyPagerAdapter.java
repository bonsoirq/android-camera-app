package com.atencjusz.projektkoncowybak.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.atencjusz.projektkoncowybak.R;

import java.util.ArrayList;

public class MyPagerAdapter extends PagerAdapter{

    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<Bitmap> bitmaps;

    public MyPagerAdapter(Context context, ArrayList<Bitmap> bitmaps) {
        this.context = context;
        this.bitmaps = bitmaps;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = layoutInflater.inflate(R.layout.layout_pager, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.pager_miniature);
        imageView.setImageBitmap(bitmaps.get(position));
        Log.d("INFALTER", "WORKING");

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    @Override
    public int getCount() {
        return bitmaps.size();
    }
}
