package com.atencjusz.projektkoncowybak.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.atencjusz.projektkoncowybak.R;
import com.atencjusz.projektkoncowybak.activities.AlbumPhotoActivity;
import com.atencjusz.projektkoncowybak.activities.ChooseAlbumActivity;
import com.atencjusz.projektkoncowybak.helpers.Networking;
import com.atencjusz.projektkoncowybak.helpers.UploadPhoto;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ListArrayAdapter extends ArrayAdapter {
    private String[] array;
    private Context context;

    public ListArrayAdapter(Context context, int resource, String[] objects) {
        super(context, resource, objects);
        array = objects;
        this.context = context;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //return super.getView(position, convertView, parent);
        //inflater - klasa konwertująca xml na kod javy
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(
                R.layout.layout_drawer,
                null);
        //szukam kontrolki w layoucie

        TextView textView = (TextView) convertView.findViewById(R.id.drawer_textView);
        textView.setText(array[position]);
        //
        ImageView imageView = (ImageView) convertView.findViewById(R.id.drawer_imageView);

        final int index = position;
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (index){
                    case 1:
                        if (!Networking.networkIsAvaiable(context)){
                            AlertDialog.Builder alert = new AlertDialog.Builder(context);
                            alert.setTitle("Error!");
                            alert.setCancelable(false);
                            alert.setMessage("Brak dostępu do sieci!");
                            alert.setNeutralButton("OK", null).show();
                            return;
                        }
                        UploadPhoto uploadPhoto = new UploadPhoto(context);
                        uploadPhoto.execute();

                        break;
                    case 2:
                        Intent share = new Intent(Intent.ACTION_SEND);
                        share.setType("image/jpeg"); //typ danych który chcemy współdzielić

                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
                        String date = dateFormat.format(new Date());

                        String tempFileName = "tmp" + date +".jpg"; // dodaj bieżąca datę do nazwy pliku

                        File tmp = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"JaroslawBak");
                        tmp = new File(tmp.getAbsolutePath() + File.separatorChar + tempFileName);

                        try {
                            FileOutputStream fs = new FileOutputStream(tmp);
                            fs.write(((AlbumPhotoActivity)context).getPhotoData());
                            fs.close();
//                            share.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/" + tempFileName)); //pobierz plik i podziel się nim:
//                            share.putExtra(Intent.EXTRA_STREAM, Uri.parse(/*"file:///sdcard/" +*/ tmp.getAbsolutePath())); //pobierz plik i podziel się nim:
                            share.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + tmp.getAbsolutePath())); //pobierz plik i podziel się nim:
                            Log.d("PATH", tmp.getAbsolutePath());
                            context.startActivity(Intent.createChooser(share, "Podziel się plikiem!")); //pokazanie okna share
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        //tmp.delete();
                        //teraz utwórz tymczasowy plik (obiekt File), który potem będzie współdzielony
                        //wpisz do niego przekonwertowaną na byte[] bitmapę pobraną ze zdjęcia (patrz poprzednie lekcje)
                        //zapisz tymczasowy plik na dysku na karcie SD w znanej sobie lokalizacji

                        break;
                }
            }
        });

        if (position == 0){
            imageView.setImageResource(R.drawable.alphabetical);
        } else if (position == 1){
            imageView.setImageResource(R.drawable.upload);
        } else if (position == 2){
        imageView.setImageResource(R.drawable.share_variant);
        }
        else imageView.setImageResource(R.drawable.folder);
        imageView.setAlpha(.53f);

        return convertView;

    }
}
