package com.atencjusz.projektkoncowybak.views;

import android.content.Context;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

/**
 * Created by 4id2 on 2016-09-29.
 */
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback{
    public CameraPreview(Context context, Camera camera) {
        super(context);
        _camera = camera;
        _surfaceHolder = this.getHolder();
        _surfaceHolder.addCallback(this);
        _camera.setDisplayOrientation(90);
    }

    private Camera _camera;
    private SurfaceHolder _surfaceHolder;

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        try {
            _camera.setPreviewDisplay(_surfaceHolder);
            _camera.startPreview();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        try {
            _camera.setPreviewDisplay(_surfaceHolder);
            _camera.startPreview();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }
}
