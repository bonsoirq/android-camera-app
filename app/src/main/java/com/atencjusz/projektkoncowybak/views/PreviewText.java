package com.atencjusz.projektkoncowybak.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.Log;
import android.util.StringBuilderPrinter;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

/**
 * Created by 4id2 on 2016-11-17.
 */
public class PreviewText extends View {

    private Paint paint;
    private String text;
    private Rect rect;
    private int textSize = 96;
    private int strokeColor = Color.RED, fillColor = Color.BLUE;
    public float offX, offY;

    public PreviewText(Context context, String text) {
        super(context);
        this.paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.paint.setAntiAlias(true);
        this.setTextSize(this.textSize);
        this.text = text;
        this.rect = new Rect();
    }

    public PreviewText(Context context, String text, Typeface typeface) {
        super(context);
        this.paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.paint.setAntiAlias(true);
        this.setTypeFace(typeface);
        this.setTextSize(this.textSize);
        this.text = text;
        this.rect = new Rect();
        //setBackgroundColor(Color.CYAN);
    }

    public PreviewText(Context context, String text, Typeface typeface, int stroke, int fill) {
        super(context);
        this.strokeColor = stroke;
        this.fillColor = fill;
        this.paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.paint.setAntiAlias(true);
        this.setTypeFace(typeface);
        this.setTextSize(this.textSize);
        this.text = text;
        this.rect = new Rect();
        //setBackgroundColor(Color.CYAN);
    }
    public PreviewText(Context context, String text, Typeface typeface, int size) {
        super(context);
        this.textSize = size;
        this.paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.paint.setAntiAlias(true);
        this.setTypeFace(typeface);
        this.setTextSize(size);
        this.text = text;
        this.rect = new Rect();
        //setBackgroundColor(Color.CYAN);
    }

    public void setTypeFace(Typeface typeface){
        paint.setTypeface(typeface);
    }
    public void setTextSize(int size){
        paint.setTextSize(size);
    }

    public Rect getRect(){
        paint.getTextBounds(PreviewText.this.text, 0, PreviewText.this.text.length(), rect);
        return this.rect;
    }

    public String getText() { return this.text; }

    public void reset(){
        paint.reset();
        this.invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        paint.setStyle(Paint.Style.FILL);
        paint.setColor(strokeColor);
        canvas.drawText(PreviewText.this.text, 0, 2*PreviewText.this.getHeight()/3, paint);

        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2);
        paint.setColor(fillColor);
        canvas.drawText(PreviewText.this.text, 0, 2*PreviewText.this.getHeight()/3, paint);
    }

    @Override
    public String toString() {
        return "PreviewText{ text='" + text + '\'' +
                '}';
    }
}
