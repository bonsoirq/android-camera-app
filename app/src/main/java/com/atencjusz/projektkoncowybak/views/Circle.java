package com.atencjusz.projektkoncowybak.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

/**
 * Created by 4id2 on 2016-10-13.
 */
public class Circle extends View {
    private Context context;
    private float radius;
    private float centerX, centerY;

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                ", centerX=" + centerX +
                ", centerY=" + centerY +
                '}';
    }

    public Circle(Context context) {
        super(context);
        this.context = context;

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
        float height = size.y;
        float width = size.x;


        this.centerX = 0;
        this.centerY = 0;
        this.radius = .3f*width;
    }

    public Circle(Context context, float radius) {
        super(context);
        this.context = context;

        this.centerX = 0;
        this.centerY = 0;
        this.radius = radius;
    }

    public void setCenterPosition(float x, float y){
        this.centerX = x;
        this.centerY = y;
        this.invalidate();
    }

    public void setRadius(float r){
        this.radius = r;
        this.invalidate();
    }

    public Circle(Context context, float X, float Y) {
        super(context);
        this.context = context;

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
        float height = size.y;
        float width = size.x;

        this.setBackgroundColor(Color.BLACK);

        this.centerX = this.getWidth()/2;
        this.centerY = this.getHeight()/2;
        this.centerX = X;
        this.centerY = Y;
        this.radius = .3f*width;
    }

    public float getRadius(){
        return this.radius;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int strokeWidth = 2;
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(strokeWidth);
        paint.setColor(Color.WHITE);

        canvas.drawCircle(this.centerX, this.centerY, this.radius, paint);
    }
}
