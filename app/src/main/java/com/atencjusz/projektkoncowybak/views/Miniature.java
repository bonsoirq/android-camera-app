package com.atencjusz.projektkoncowybak.views;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.atencjusz.projektkoncowybak.activities.CameraActivity;

public class Miniature extends ImageView {

    private Context context;
    private Bitmap bitmap;
    private byte[] photoData;
    private int size;
    private int positionX, positionY;

    public Miniature(Context context) {
        super(context);
        this.context = context;
        this.rotateBitmap();
        this.initListeners();
    }

    public Miniature(Context context, byte[] photoData) {
        super(context);
        this.context = context;
        this.photoData = photoData;
        this.bitmap = BitmapFactory.decodeByteArray(photoData, 0, photoData.length);
        this.rotateBitmap();
        this.initListeners();
    }

    public Miniature(Context context, byte[] photoData, int size) {
        super(context);
        this.context = context;
        this.photoData = photoData;
        this.bitmap = BitmapFactory.decodeByteArray(photoData, 0, photoData.length);
        this.setSize(size);
        this.setBackgroundColor(Color.RED);
        this.rotateBitmap();
        this.initListeners();
    }

    public void setSize(int size){
        this.setLayoutParams(new LinearLayout.LayoutParams(size,size));
        this.bitmap = Bitmap.createScaledBitmap(this.bitmap, size, size, false);
        this.size = size;
    }

    public void setSize(int width, int height){
        this.bitmap = Bitmap.createScaledBitmap(this.bitmap, width, height, false);
    }

    public void setCenterPosition(int x, int y){
        this.positionX = x - (this.size/2);
        this.positionY = y - (this.size/2);
        this.setX(x + (this.size/2));
        this.setY(y + (this.size/2));
    }

    public void setCenterPosition(double x, double y){
        this.positionX = ((int) x) - (this.size/2);
        this.positionY = ((int) y) - (this.size/2);
        this.setX(((float) x) - (this.size/2));
        this.setY(((float) y) - (this.size/2));
    }

    public String printPosition(){
        return this.positionX + ":"+this.positionY;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int strokeWidth = 2;
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(strokeWidth);
        paint.setColor(Color.WHITE);

        canvas.drawBitmap(this.bitmap, 0,0,paint);
        canvas.drawRect(0,
                0,
                this.size,
                this.size,
                paint);


    }

    private void rotateBitmap(){
        Matrix matrix = new Matrix();
        // rotate Bitmap
        matrix.postRotate(90);
        this.bitmap = Bitmap.createBitmap(this.bitmap,0,0,this.size,this.size, matrix,true);
    }

    private void rotateBitmap(Bitmap bitmap){
        Matrix matrix = new Matrix();
        // rotate Bitmap
        matrix.postRotate(90);
        bitmap = Bitmap.createBitmap(this.bitmap,0,0,this.size,this.size, matrix,true);
    }

    private void initListeners(){
        this.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Wybierz akcję");
                //cannot have setMessage

                final String[] options ={
                        "Pogląd",
                        "Usuń",
                        "Usuń wszystkie",
                        "Zapisz",
                        "Zapisz wszystkie"
                };

                alertDialog.setItems(options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int index) {
                        //display options[which]);
                        Handler handler = new Handler();
                        switch (index){
                            case 0:
                                ((CameraActivity)context).showInfo("Podgląd...");
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //preview
                                        Bitmap bitmap = BitmapFactory.decodeByteArray(photoData, 0, photoData.length);
                                        ((CameraActivity)context).showPhotoPreview(bitmap);
                                        ((CameraActivity)context).hideInfo();
                                    }
                                }, 1);
                                break;
                            case 1:
                                ((CameraActivity)context).showInfo("Usuwanie...");
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //delete one bitmap
                                        ((CameraActivity)context).deleteMiniature(Miniature.this);
                                        ((CameraActivity)context).hideInfo();
                                    }
                                }, 1);
                                break;
                            case 2:
                                ((CameraActivity)context).showInfo("Usuwanie...");
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //delete one bitmap
                                        ((CameraActivity)context).deleteAllMiniatures();
                                        ((CameraActivity)context).hideInfo();
                                    }
                                }, 1);
                                break;
                            case 3:
                                ((CameraActivity)context).showInfo("Zapisywanie...");
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //delete one bitmap
                                        ((CameraActivity)context).saveImage(Miniature.this);
                                        ((CameraActivity)context).hideInfo();
                                    }
                                }, 1);
                                break;
                            case 4:
                                ((CameraActivity)context).showInfo("Zapisywanie...");
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //delete one bitmap
                                        ((CameraActivity)context).saveAllImages();
                                        ((CameraActivity)context).hideInfo();
                                    }
                                }, 1);
                                break;
                            default:
                        }
                    }
                });
                alertDialog.show();
                return false;
            }
        });

        this.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()){
                    case MotionEvent.ACTION_MOVE:
                        if (motionEvent.getRawX() - Miniature.this.getX() > 2*Miniature.this.size){
                            Handler handler = new Handler();
                            ((CameraActivity)context).showInfo("Usuwanie...");
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //delete one bitmap
                                    ((CameraActivity)context).deleteMiniature(Miniature.this);
                                    ((CameraActivity)context).hideInfo();
                                }
                            }, 1);
                        }
                        break;
                    default:
                        break;
                }
                return false;
            }
        });
    }


}
