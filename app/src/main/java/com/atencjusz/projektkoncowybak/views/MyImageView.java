package com.atencjusz.projektkoncowybak.views;

import android.content.Context;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

/**
 * Created by 4id2 on 2016-09-22.
 */
public class MyImageView extends ImageView {
    public MyImageView(Context context, boolean isWide) {
        super(context);
        _context = context;
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        display = wm.getDefaultDisplay();

        if (isWide)
            layoutParams = new LinearLayout.LayoutParams(3 * display.getWidth() / 5, display.getHeight()/5);
        else
            layoutParams = new LinearLayout.LayoutParams(2 * display.getWidth() / 5, display.getHeight()/5);

        this.setLayoutParams(layoutParams);
    }
    private Display display;
    private Context _context;
    private LinearLayout.LayoutParams layoutParams;
}
