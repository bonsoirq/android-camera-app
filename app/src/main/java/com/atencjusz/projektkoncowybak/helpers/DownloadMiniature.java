package com.atencjusz.projektkoncowybak.helpers;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.atencjusz.projektkoncowybak.activities.MainActivity;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class DownloadMiniature extends AsyncTask<String, Void, String> {

    private Context context;
    private String name;
    private int position, length;

    public DownloadMiniature(Context context, String name, int position, int length) {
        this.context = context;
        this.name = name;
        this.position = position;
        this.length = length;
    }

    @Override
    protected String doInBackground(String... strings) {
        /*AsyncHttpClient httpClient = new AsyncHttpClient() {
            @Override
            public boolean isClosed() {
                return false;
            }

            @Override
            public AsyncHttpClient setSignatureCalculator(SignatureCalculator signatureCalculator) {
                return null;
            }

            @Override
            public BoundRequestBuilder prepareGet(String s) {
                return null;
            }

            @Override
            public BoundRequestBuilder prepareConnect(String s) {
                return null;
            }

            @Override
            public BoundRequestBuilder prepareOptions(String s) {
                return null;
            }

            @Override
            public BoundRequestBuilder prepareHead(String s) {
                return null;
            }

            @Override
            public BoundRequestBuilder preparePost(String s) {
                return null;
            }

            @Override
            public BoundRequestBuilder preparePut(String s) {
                return null;
            }

            @Override
            public BoundRequestBuilder prepareDelete(String s) {
                return null;
            }

            @Override
            public BoundRequestBuilder preparePatch(String s) {
                return null;
            }

            @Override
            public BoundRequestBuilder prepareTrace(String s) {
                return null;
            }

            @Override
            public BoundRequestBuilder prepareRequest(Request request) {
                return null;
            }

            @Override
            public BoundRequestBuilder prepareRequest(RequestBuilder requestBuilder) {
                return null;
            }

            @Override
            public <T> ListenableFuture<T> executeRequest(Request request, AsyncHandler<T> asyncHandler) {
                return null;
            }

            @Override
            public <T> ListenableFuture<T> executeRequest(RequestBuilder requestBuilder, AsyncHandler<T> asyncHandler) {
                return null;
            }

            @Override
            public ListenableFuture<Response> executeRequest(Request request) {
                return null;
            }

            @Override
            public ListenableFuture<Response> executeRequest(RequestBuilder requestBuilder) {
                return null;
            }

            @Override
            public void close() throws IOException {

            }
        }; // klient http

        Future<Response> httpResponse = null; // obiekt odpowiedzi z serwera
        String result = "";
        try {
            httpResponse = httpClient.preparePost(Prefs.SERVER_URL + "/img/miniatures/" + name).execute(); // wykonanie wysłania
            result = httpResponse.get().toString(); // odebranie odpowiedzi z serwera, którą potem wyświetlimy w onPostExecute

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        Prefs.PAGER_BITMAPS.add(ImageProcessing.createBitmap(result.getBytes()));
        return result.toString();*/
        return "";
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Log.d("MINI", s + Integer.toString(position)+Integer.toString(length));
        if (position == length) {
            ((MainActivity)context).setPagerAdapter();
        }
    }
}
