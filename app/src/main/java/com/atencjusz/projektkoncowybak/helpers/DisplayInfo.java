package com.atencjusz.projektkoncowybak.helpers;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

public class DisplayInfo extends View {

    private Context context;
    private WindowManager windowManager;
    private Display display;
    private Point point;

    public DisplayInfo(Context context) {
        super(context);
        this.context = context;
        this.windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        this.display = this.windowManager.getDefaultDisplay();
        this.point = new Point();
        this.display.getSize(this.point);
    }


    public float getDisplayWidth(){ return this.point.x; }
    public float getDisplayHeight(){ return this.point.y; }
}
