package com.atencjusz.projektkoncowybak.helpers;

import android.graphics.Bitmap;
import android.graphics.Typeface;

import com.atencjusz.projektkoncowybak.views.PreviewText;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by 4id2 on 2016-10-06.
 */
public class Prefs {
    public static String getSelectedAlbum() {
        return SELECTED_ALBUM;
    }

    public static void setSelectedAlbum(String selectedAlbum) {
        SELECTED_ALBUM = selectedAlbum;
    }

    private static String SELECTED_ALBUM;

    public static byte[] getPhotoData() {
        return PHOTO_DATA;
    }

    public static void setPhotoData(byte[] photoData) {
        PHOTO_DATA = photoData;
    }

    private static byte[] PHOTO_DATA;

    public static Bitmap getPreviewPhoto() {
        return PREVIEW_PHOTO;
    }

    public static void setPreviewPhoto(Bitmap previewPhoto) {
        PREVIEW_PHOTO = previewPhoto;
    }

    private static Bitmap PREVIEW_PHOTO;

    private static String PREVIEW_TEXT;

    public static String getPreviewText() {
        return PREVIEW_TEXT;
    }

    public static void setPreviewText(String previewText) {
        PREVIEW_TEXT = previewText;
    }

    public static Typeface getPreviewTypeface() {
        return PREVIEW_TYPEFACE;
    }

    public static void setPreviewTypeface(Typeface previewTypeface) {
        PREVIEW_TYPEFACE = previewTypeface;
    }

    private static Typeface PREVIEW_TYPEFACE;

    public static int getCOLOR() {
        return COLOR;
    }

    public static void setCOLOR(int COLOR) {
        Prefs.COLOR = COLOR;
    }

    private static int COLOR;
    private static int STROKE;
    private static int FILL;

    public static int getSTROKE() {
        return STROKE;
    }

    public static void setSTROKE(int STROKE) {
        Prefs.STROKE = STROKE;
    }

    public static int getFILL() {
        return FILL;
    }

    public static void setFILL(int FILL) {
        Prefs.FILL = FILL;
    }

    public static final String SERVER_URL = "192.168.0.2";

    public static final ArrayList<Bitmap> PAGER_BITMAPS = new ArrayList<>();

    public static String JSON;


}
