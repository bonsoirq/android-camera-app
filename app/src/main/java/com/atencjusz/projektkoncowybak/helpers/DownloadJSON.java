package com.atencjusz.projektkoncowybak.helpers;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.atencjusz.projektkoncowybak.activities.AlbumPhotoActivity;
import com.atencjusz.projektkoncowybak.activities.MainActivity;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class DownloadJSON extends AsyncTask<String, Void, String> {

    private Context context;

    public DownloadJSON(Context context) {
        this.context = context;
    }


    @Override
    protected String doInBackground(String... strings) {
        URI address = null;
        try {
            address = new URI("http", null, Prefs.SERVER_URL, 8080, "/json", "", "");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        HttpPost httpPost = new HttpPost(address); // URL_SERWERA proponuję zapisać w osobnej klasie np Settings w postaci stałej
        DefaultHttpClient httpClient = new DefaultHttpClient(); // klient http
        HttpResponse httpResponse = null; // obiekt odpowiedzi z serwera
        String result = "";
        try {
            httpResponse = httpClient.execute(httpPost); // wykonanie wysłania
            result = EntityUtils.toString(httpResponse.getEntity(), HTTP.UTF_8); // odebranie odpowiedzi z serwera, którą potem wyświetlimy w onPostExecute
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
//        Log.d("JSON", s);
        Prefs.JSON = s;
        ((MainActivity)context).downloadBitmaps();
        Log.d("PAGER", s);
    }
}
