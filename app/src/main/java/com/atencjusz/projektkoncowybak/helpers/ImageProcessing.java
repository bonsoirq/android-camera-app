package com.atencjusz.projektkoncowybak.helpers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

import java.io.FileOutputStream;

/**
 * Created by 4id2 on 2016-10-13.
 */
public class ImageProcessing {

    public static Bitmap rotateBitmap(Bitmap bitmap){
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            return Bitmap.createBitmap(
                    bitmap,
                    0,
                    0,
                    bitmap.getWidth(),
                    bitmap.getHeight(),
                    matrix,
                    true
            );
    }

    public static Bitmap flipBitmap(Bitmap bitmap){
        Matrix matrix = new Matrix();
        matrix.postScale(-1.0f, 1.0f);
        return Bitmap.createBitmap(
                bitmap,
                0,
                0,
                bitmap.getWidth(),
                bitmap.getHeight(),
                matrix,
                true
        );
    }

    public static Bitmap rotateBitmap(Bitmap bitmap, int rotation){
        Matrix matrix = new Matrix();
        matrix.postRotate(rotation);
        return Bitmap.createBitmap(
                bitmap,
                0,
                0,
                bitmap.getWidth(),
                bitmap.getHeight(),
                matrix,
                Boolean.TRUE
        );
    }

    public static Bitmap saveBitmapToJPEG(FileOutputStream fileOutputStream, Bitmap bitmap){
        bitmap.compress(
                Bitmap.CompressFormat.JPEG,
                50,
                fileOutputStream
        );
        return bitmap;
    }

    public static void saveBitmapToJPEG(FileOutputStream fileOutputStream, Bitmap bitmap, int quality){
        bitmap.compress(
                Bitmap.CompressFormat.JPEG,
                quality,
                fileOutputStream
        );
    }

    public  static Bitmap createBitmap(byte[] data){
        return  BitmapFactory.decodeByteArray(data, 0, data.length);
    }
}
