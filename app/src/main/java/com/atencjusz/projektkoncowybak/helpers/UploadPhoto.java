package com.atencjusz.projektkoncowybak.helpers;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.atencjusz.projektkoncowybak.activities.AlbumPhotoActivity;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class UploadPhoto extends AsyncTask<String, Void, String> {

    private ProgressDialog progressDialog;
    private Context context;
    private byte[] data;

    public UploadPhoto(Context context) {
        this.context = context;
    }

    public UploadPhoto(Context context, byte[] data) {
        this.context = context;
        this.data = data;
    }

    @Override
    protected String doInBackground(String... strings) {
        URI address = null;
        try {
            address = new URI("http", null, Prefs.SERVER_URL, 8080, "", "", "");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        HttpPost httpPost = new HttpPost(address); // URL_SERWERA proponuję zapisać w osobnej klasie np Settings w postaci stałej
        httpPost.setEntity(new ByteArrayEntity(((AlbumPhotoActivity)context).getPhotoData())); // bytes - nasze zdjęcie przekonwertowane na byte[]
        DefaultHttpClient httpClient = new DefaultHttpClient(); // klient http
        HttpResponse httpResponse = null; // obiekt odpowiedzi z serwera
        String result = "";
        try {
            httpResponse = httpClient.execute(httpPost); // wykonanie wysłania
            result = EntityUtils.toString(httpResponse.getEntity(), HTTP.UTF_8); // odebranie odpowiedzi z serwera, którą potem wyświetlimy w onPostExecute
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = ProgressDialog.show(this.context, "Wysyłanie",
                "Trwa upload zdjęcia", true);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        progressDialog.dismiss();
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle("Sukces!");
        alert.setCancelable(false);
        alert.setMessage(s);
        alert.setNeutralButton("OK", null).show();
    }
}
