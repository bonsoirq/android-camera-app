package com.atencjusz.projektkoncowybak.activities;

import android.content.Intent;
import android.os.Environment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.atencjusz.projektkoncowybak.adapters.MyPagerAdapter;
import com.atencjusz.projektkoncowybak.helpers.DownloadJSON;
import com.atencjusz.projektkoncowybak.helpers.DownloadMiniature;
import com.atencjusz.projektkoncowybak.helpers.ImageData;
import com.atencjusz.projektkoncowybak.helpers.Networking;
import com.atencjusz.projektkoncowybak.helpers.Prefs;
import com.atencjusz.projektkoncowybak.R;
import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private MyPagerAdapter myPagerAdapter;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();


        /*
         * handle pictures directory
         */
        File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        if (!file.exists()){
            file.mkdir();
        }

        //if my album dir doesn't exist create it
        File dir = new File(file,"JaroslawBak");
        if (!dir.exists()){
            dir.mkdir();
            //fill with subdirectories
            File[] albums = new File[3];
            for (int i = 0; i < 3; i++){
                albums[i] = new File(dir,"album"+i);
                albums[i].mkdir();
            }
        }

        /*
         * particular click event listeners
         */

        //region albums
        ImageView imageView = (ImageView)findViewById(R.id.activity_main_albums);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,AlbumsActivity.class);
                startActivity(intent);
            }
        });
        //endregion

        //region camera
        imageView = (ImageView)findViewById(R.id.activity_main_choose_album);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Prefs.getSelectedAlbum() != null) {
                    Intent intent = new Intent(MainActivity.this, CameraActivity.class);
                    Bundle b = new Bundle();
                    b.putString("albumname", Prefs.getSelectedAlbum());
                    intent.putExtras(b);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(MainActivity.this, ChooseAlbumActivity.class);
                    startActivity(intent);
                }
            }
        });
        //endregion

        //region collage
        imageView = (ImageView)findViewById(R.id.activity_main_collage);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this, ChooseCollageActivity.class);
                    startActivity(intent);
            }
        });
        //endregion

        //region network

        if (Networking.networkIsAvaiable(MainActivity.this)){
            viewPager = (ViewPager) findViewById(R.id.pager);
            DownloadJSON downloadJSON = new DownloadJSON(MainActivity.this);
            downloadJSON.execute();
            //myPagerAdapter = new MyPagerAdapter(MainActivity.this);
        }

        imageView = (ImageView)findViewById(R.id.activity_main_network);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ColorPickerActivity.class);
                startActivity(intent);
            }
        });
        //endregion


    }

    public void downloadBitmaps(){
        Prefs.PAGER_BITMAPS.clear();
        ImageData[] imageDatas = new Gson().fromJson(Prefs.JSON, ImageData[].class);
        for ( int i = 0; i < imageDatas.length; i++){
            DownloadMiniature dm = new DownloadMiniature(MainActivity.this, imageDatas[i].name, i, imageDatas.length - 1);
            dm.execute();
        }

    }

    public void setPagerAdapter(){
        Log.d("BITMAPS", Prefs.PAGER_BITMAPS.toString());
        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(MainActivity.this, Prefs.PAGER_BITMAPS);
        viewPager.setAdapter(myPagerAdapter);
    }
}
