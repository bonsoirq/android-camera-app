package com.atencjusz.projektkoncowybak.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.atencjusz.projektkoncowybak.helpers.Prefs;
import com.atencjusz.projektkoncowybak.R;

import java.util.ArrayList;

public class ImagesScrollViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images_scroll_view);
        getSupportActionBar().hide();

        Bundle bundle = this.getIntent().getExtras();
        ArrayList<String> fileNames = bundle.getStringArrayList("filenames");
        String albumPath = (String)bundle.get("albumpath");

        WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        LinearLayout container = (LinearLayout)findViewById(R.id.images_scroll_view);
        LinearLayout row = new LinearLayout(ImagesScrollViewActivity.this);


        int wide = 3 * display.getWidth() / 5,
                narrow = 2 * display.getWidth() / 5,
                height = display.getHeight()/5;
        for (int i = 0, length = fileNames.size(); i < length; i++){

            if (i%2 == 0){
                row = new LinearLayout(ImagesScrollViewActivity.this);
                container.addView(row);
            }
            if (length%2 != 0 && i+1 == length){
                ImageView imageView = new ImageView(ImagesScrollViewActivity.this);
                imageView.setLayoutParams(new LinearLayout.LayoutParams(display.getWidth(), height));
                Bitmap bmp = betterImageDecode(albumPath + fileNames.get(i));
                imageView.setImageBitmap(bmp);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                row.addView(imageView);
                final ImageView finalImageView = imageView;
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(ImagesScrollViewActivity.this, AlbumPhotoActivity.class);
                        Prefs.setPreviewPhoto(((BitmapDrawable) finalImageView.getDrawable()).getBitmap());
                        startActivity(intent);
                    }
                });
                break;
            }
            ImageView imageView = new ImageView(ImagesScrollViewActivity.this);
            switch (i%4){
                case 0:
                    imageView = new ImageView(ImagesScrollViewActivity.this);
                    imageView.setLayoutParams(new LinearLayout.LayoutParams(wide, height));
                    break;
                case 1:
                    imageView = new ImageView(ImagesScrollViewActivity.this);
                    imageView.setLayoutParams(new LinearLayout.LayoutParams(narrow, height));
                    break;
                case 2:
                    imageView = new ImageView(ImagesScrollViewActivity.this);
                    imageView.setLayoutParams(new LinearLayout.LayoutParams(narrow, height));
                    break;
                case 3:
                    imageView = new ImageView(ImagesScrollViewActivity.this);
                    imageView.setLayoutParams(new LinearLayout.LayoutParams(wide, height));
                    break;
            }
            Bitmap bmp = betterImageDecode(albumPath + fileNames.get(i));
            imageView.setImageBitmap(bmp);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            row.addView(imageView);

            final ImageView finalImageView = imageView;
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(ImagesScrollViewActivity.this, AlbumPhotoActivity.class);
                    Prefs.setPreviewPhoto(((BitmapDrawable) finalImageView.getDrawable()).getBitmap());
                    startActivity(intent);
                }
            });
        }

        /*int imgSizeSwitch = 0;
        LinearLayout scrollView = (LinearLayout) findViewById(R.id.images_scroll_view);
        LinearLayout row = new LinearLayout(ImagesScrollViewActivity.this);

        for (String filename : filenames){
            MyImageView imageView;
            switch (imgSizeSwitch%4){
                case 0:
                    row = new LinearLayout(ImagesScrollViewActivity.this);
                    imageView = new MyImageView(ImagesScrollViewActivity.this, true);
                    imgSizeSwitch++;
                    break;
                case 1:
                    imageView = new MyImageView(ImagesScrollViewActivity.this, false);
                    scrollView.addView(row);
                    imgSizeSwitch++;
                    break;
                case 2:
                    row = new LinearLayout(ImagesScrollViewActivity.this);
                    imageView = new MyImageView(ImagesScrollViewActivity.this, false);
                    imgSizeSwitch++;
                    break;
                case 3:
                    imageView = new MyImageView(ImagesScrollViewActivity.this, true);
                    scrollView.addView(row);
                    imgSizeSwitch = 0;
                    break;
                default:
                    imageView = new MyImageView(ImagesScrollViewActivity.this, true);
            }
            String imagepath = bundle.get("albumpath")+ filename;
            Bitmap bmp = betterImageDecode(imagepath);
            imageView.setImageBitmap(bmp);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            row.addView(imageView);
        }*/
    }

    private Bitmap betterImageDecode(String filePath) {

        Bitmap myBitmap;
        BitmapFactory.Options options = new BitmapFactory.Options();    //opcje przekształcania bitmapy
        options.inSampleSize = 4; // zmniejszenie jakości bitmapy 4x
        //
        myBitmap = BitmapFactory.decodeFile(filePath, options);
        return myBitmap;
    }
}
