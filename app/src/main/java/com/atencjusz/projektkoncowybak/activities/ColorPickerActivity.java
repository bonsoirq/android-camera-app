package com.atencjusz.projektkoncowybak.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.atencjusz.projektkoncowybak.R;
import com.atencjusz.projektkoncowybak.helpers.Prefs;
import com.atencjusz.projektkoncowybak.views.ImageData;

public class ColorPickerActivity extends AppCompatActivity {

    private ImageView colorPicker,
    accept;
    private LinearLayout preview;
    private int req;
    private int color;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color_picker);
        getSupportActionBar().hide();
        req = getIntent().getIntExtra("req", 0x0A);
        colorPicker = (ImageView) findViewById(R.id.colorpicker_picker);
        accept = (ImageView) findViewById(R.id.colorpicker_accept);
        preview = (LinearLayout) findViewById(R.id.colorpicker_preview);

        colorPicker.setDrawingCacheEnabled(true);
        colorPicker.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()){
                    case MotionEvent.ACTION_MOVE:
                        Bitmap bmp = view.getDrawingCache();
                        //pobranie koloru piksela z odpowiedniego miejsca bitmapy
                        color = bmp.getPixel((int)motionEvent.getX(), (int)motionEvent.getY());

                        //tu sprawdzaj i ustawiaj na bieżąco podgląd koloru:
                        preview.setBackgroundColor(color);
                        //Log.d("kolor", Integer.toString(color));
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("color", color);
                Prefs.setCOLOR(color);
                setResult(req);
                finish();
            }
        });
    }
}
