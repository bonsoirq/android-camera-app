package com.atencjusz.projektkoncowybak.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.atencjusz.projektkoncowybak.views.CameraPreview;
import com.atencjusz.projektkoncowybak.helpers.Prefs;
import com.atencjusz.projektkoncowybak.R;

import java.util.List;

public class CollageCameraActivity extends AppCompatActivity {

    private byte[] photoData;
    private FrameLayout frameLayout;

    private ImageView ivAccept,
            ivCancel,
            ivChangeFacing,
            ivCamera,
            ivFilters,
            ivExposure,
            ivWhiteBalance,
            ivResolution;

    //camera
    private Camera camera;
    private int cameraId = -1;
    private CameraPreview _cameraPreview;
    private FrameLayout _frameLayout;
    private Camera.Parameters cameraParameters;
    private Camera.PictureCallback cameraPictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            // store photo data in byte[]
            // use it later
            // saving picture after user confirm taken photo
//            photoData = data;
            Prefs.setPhotoData(data);

            // refresh camera
            camera.stopPreview();
            //camera.startPreview();
        }
    };
    private List<String> whiteBalanceList;
    private List<Camera.Size> pictureSizeList;
    private List<String> colorEffectsList;
    private int minExposure;
    private int maxExposure;
    private int currentExposure;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collage_camera);
        getSupportActionBar().hide();
        initCamera();
        initPreview();
        initViewFields();
        initOnClickEventListeners();
    }
    @Override
    protected void onPause(){
        super.onPause();
        if (camera != null) {
            camera.stopPreview();
            //linijka nieudokumentowana w API, bez niej jest crash przy wznawiamiu kamery
            _cameraPreview.getHolder().removeCallback(_cameraPreview);
            camera.release();
            camera = null;
        }
    }

    @Override
    protected void onResume(){
        super.onResume();

        if (camera == null) {
            initCamera();
            initPreview();
            camera.startPreview();
        }
    }

    private void initCamera(){
        boolean cameraSupported = getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);

        if (!cameraSupported) {
            // no camera
        } else {
            // wykorzystanie danych zwróconych przez kolejną funkcję getCameraId
            cameraId = getCameraId();
            // cam supported
            if (cameraId < 0) {
                // no selfie camera
            } else if (cameraId >= 0) {
                camera = Camera.open(cameraId);
                cameraParameters = camera.getParameters();
                whiteBalanceList = cameraParameters.getSupportedWhiteBalance();
                pictureSizeList = cameraParameters.getSupportedPictureSizes();
                colorEffectsList = cameraParameters.getSupportedColorEffects();

                minExposure = cameraParameters.getMinExposureCompensation();
                maxExposure = cameraParameters.getMaxExposureCompensation();
                currentExposure = cameraParameters.getExposureCompensation();
            }
        }
    }

    private void initPreview(){
        _cameraPreview = new CameraPreview(CollageCameraActivity.this, camera);
        _frameLayout = (FrameLayout) findViewById(R.id.frameLayoutCollageCameraActivity);
        _frameLayout.addView(_cameraPreview);

    }

    private int getCameraId(){
        int cid = 0;
        int camerasCount = Camera.getNumberOfCameras(); // when more than one camera

        for (int i = 0; i < camerasCount; i++) {
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            Camera.getCameraInfo(i, cameraInfo);

            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cid = i;
            }
	    /*
            if (cameraInfo.facing == CameraInfo.CAMERA_FACING_FRONT) {
                cid = i;
            }
	    */
        }

        return cid;
    }

    private void initViewFields(){

        this.frameLayout = (FrameLayout) findViewById(R.id.frameLayoutCollageCameraActivity);
        this.ivAccept = (ImageView)findViewById(R.id.collageCameraCheck);
        this.ivCancel = (ImageView)findViewById(R.id.collageCameraCancel);
        this.ivChangeFacing = (ImageView)findViewById(R.id.collageCameraFacing);
        this.ivCamera = (ImageView)findViewById(R.id.collageCameraTakePicture);
        this.ivFilters = (ImageView)findViewById(R.id.collageCameraFilters);
        this.ivExposure = (ImageView)findViewById(R.id.collageCameraExposure);
        this.ivWhiteBalance = (ImageView)findViewById(R.id.collageCameraWhiteBalance);
        this.ivResolution = (ImageView)findViewById(R.id.collageCameraResolution);
    }

    private void initOnClickEventListeners() {
        final LinearLayout llAccept = (LinearLayout) ivAccept.getParent(),
                llCancel = (LinearLayout) ivCancel.getParent(),
                llFacing = (LinearLayout) ivChangeFacing.getParent(),
                llCamera = (LinearLayout) ivCamera.getParent();


        //DISABLED DUE TO TESTING OTHER FEATURES
        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llAccept.setVisibility(View.VISIBLE);
                llCancel.setVisibility(View.VISIBLE);
                llFacing.setVisibility(View.GONE);
                llCamera.setVisibility(View.GONE);
                camera.takePicture(null, null, cameraPictureCallback);

                ivAccept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
//                        intent.putExtra("photodata", photoData);
                        setResult(0x0B, intent);   // 0x0B
                        finish();

                    }
                });

                ivCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        llAccept.setVisibility(View.GONE);
                        llCancel.setVisibility(View.GONE);
                        llFacing.setVisibility(View.VISIBLE);
                        llCamera.setVisibility(View.VISIBLE);
                        camera.startPreview();
                    }
                });


            }
        });

        ivFilters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(CollageCameraActivity.this);
                alertDialog.setTitle("Color effects");
                //cannot have setMessage
                String[] tmp = new String[colorEffectsList.size()];

                for (int i = 0, length = colorEffectsList.size(); i < length; i++){
                    tmp[i] = colorEffectsList.get(i);
                }

                final String[] options = tmp;
                alertDialog.setItems(options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int index) {
                        //display options[which]);
                        cameraParameters.setColorEffect(options[index]);
                        camera.setParameters(cameraParameters);
                    }
                });
                alertDialog.show();
            }
        });

        ivExposure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(CollageCameraActivity.this);
                alertDialog.setTitle("Exposure");
                int tmpLength = maxExposure - minExposure;
                String[] tmp = new String[tmpLength];

                for (int i = 0; i < tmpLength; i++){
                    tmp[i] = (minExposure + i) + "";
                }

                final String[] options = tmp;
                alertDialog.setItems(options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int index) {;
                        int tmp = Integer.parseInt(options[index]);
                        cameraParameters.setExposureCompensation(tmp);
                        camera.setParameters(cameraParameters);
                    }
                });
                alertDialog.show();
            }
        });

        ivWhiteBalance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(CollageCameraActivity.this);
                alertDialog.setTitle("White balance");
                String[] tmp = new String[whiteBalanceList.size()];
                int i = 0;
                for (String s : whiteBalanceList){
                    tmp[i] = s;
                    i++;
                }
                final String[] options = tmp;
                alertDialog.setItems(options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int index) {
                        cameraParameters.setWhiteBalance(options[index]);
                        camera.setParameters(cameraParameters);
                    }
                });
                alertDialog.show();
            }
        });

        ivResolution.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(CollageCameraActivity.this);
                alertDialog.setTitle("Resolution");
                //cannot have setMessage
                String[] tmp = new String[pictureSizeList.size()];
                int i = 0;
                for (Camera.Size s : pictureSizeList){
                    tmp[i] = s.width + "x" + s.height;
                    i++;
                }
                final String[] options = tmp;
                alertDialog.setItems(options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int index) {
                        //display options[which]);
                        cameraParameters.setPictureSize(pictureSizeList.get(index).width,pictureSizeList.get(index).height);
                        camera.setParameters(cameraParameters);
                    }
                });
                alertDialog.show();
            }
        });
    }
}
