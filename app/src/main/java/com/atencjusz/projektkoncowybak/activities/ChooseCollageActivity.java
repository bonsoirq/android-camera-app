package com.atencjusz.projektkoncowybak.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.atencjusz.projektkoncowybak.helpers.DisplayInfo;
import com.atencjusz.projektkoncowybak.views.ImageData;
import com.atencjusz.projektkoncowybak.R;

import java.util.ArrayList;

public class ChooseCollageActivity extends AppCompatActivity {

    private ArrayList<ImageData> imageDatas;
    private DisplayInfo displayInfo;
    private ImageView simple, medium, advanced;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_collage);
        getSupportActionBar().hide();

        this.initFields();
        this.initListeners();

    }

    private void initFields(){
        this.simple = (ImageView) findViewById(R.id.collage_simple);
        this.medium = (ImageView) findViewById(R.id.collage_medium);
        this.advanced = (ImageView) findViewById(R.id.collage_advanced);
        this.imageDatas = new ArrayList<>();
        this.displayInfo = new DisplayInfo(ChooseCollageActivity.this);
//        this.displayInfo.setLayoutParams(new LinearLayout.LayoutParams(this.displayInfo.getWidth(),9*this.displayInfo.getHeight()/10));
    }

    private void initListeners(){
        this.simple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageDatas.clear();
                imageDatas.add(new ImageData(
                                0,
                                0,
                                displayInfo.getDisplayWidth(),
                                2*displayInfo.getDisplayHeight()/3
                        )
                );
                imageDatas.add(new ImageData(
                                0,
                                2*displayInfo.getDisplayHeight()/3 + 1,
                                displayInfo.getDisplayWidth()/2,
                                displayInfo.getDisplayHeight()/3
                        )
                );
                imageDatas.add(new ImageData(
                                displayInfo.getDisplayWidth()/2 + 1,
                                2*displayInfo.getDisplayHeight()/3 + 1,
                                displayInfo.getDisplayWidth()/2,
                                displayInfo.getDisplayHeight()/3
                        )
                );

                Intent intent = new Intent(ChooseCollageActivity.this, CollageActivity.class);
                intent.putExtra("list", imageDatas);
                startActivity(intent);
            }
        });

        this.medium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageDatas.clear();
                imageDatas.add(new ImageData(
                                0,
                                0,
                                displayInfo.getDisplayWidth()/2,
                                displayInfo.getDisplayHeight()
                        )
                );
                imageDatas.add(new ImageData(
                                displayInfo.getDisplayWidth()/2 + 1,
                                0,
                                displayInfo.getDisplayWidth()/2,
                                displayInfo.getDisplayHeight()/3
                        )
                );
                imageDatas.add(new ImageData(
                                displayInfo.getDisplayWidth()/2 + 1,
                                displayInfo.getDisplayHeight()/3 + 1,
                                displayInfo.getDisplayWidth()/2,
                                displayInfo.getDisplayHeight()/3
                        )
                );
                imageDatas.add(new ImageData(
                                displayInfo.getDisplayWidth()/2 + 1,
                                2*displayInfo.getDisplayHeight()/3 + 2,
                                displayInfo.getDisplayWidth()/2,
                                displayInfo.getDisplayHeight()/3
                        )
                );

                Intent intent = new Intent(ChooseCollageActivity.this, CollageActivity.class);
                intent.putExtra("list", imageDatas);
                startActivity(intent);
            }
        });

        this.advanced.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageDatas.clear();
                imageDatas.add(new ImageData(
                                0,
                                0,
                                displayInfo.getDisplayWidth()/6,
                                displayInfo.getDisplayHeight()/3
                        )
                );
                imageDatas.add(new ImageData(
                                displayInfo.getDisplayWidth()/6 + 1,
                                0,
                                displayInfo.getDisplayWidth()/2,
                                displayInfo.getDisplayHeight()/3
                        )
                );
                imageDatas.add(new ImageData(
                                2*displayInfo.getDisplayWidth()/3 + 2,
                                0,
                                displayInfo.getDisplayWidth()/3,
                                2*displayInfo.getDisplayHeight()/3 + 1
                        )
                );
                imageDatas.add(new ImageData(
                                0,
                                displayInfo.getDisplayHeight()/3 + 1,
                                2*displayInfo.getDisplayWidth()/3 + 1,
                                displayInfo.getDisplayHeight()/3
                        )
                );
                imageDatas.add(new ImageData(
                                0,
                                2*displayInfo.getDisplayHeight()/3 + 2,
                                displayInfo.getDisplayWidth()/3,
                                displayInfo.getDisplayHeight()/3
                        )
                );
                imageDatas.add(new ImageData(
                                displayInfo.getDisplayWidth()/3 +1,
                                2*displayInfo.getDisplayHeight()/3 + 2,
                                2*displayInfo.getDisplayWidth()/3,
                                displayInfo.getDisplayHeight()/3
                        )
                );

                Intent intent = new Intent(ChooseCollageActivity.this, CollageActivity.class);
                intent.putExtra("list", imageDatas);
                startActivity(intent);
            }
        });
    }
}
