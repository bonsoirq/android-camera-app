package com.atencjusz.projektkoncowybak.activities;

import android.animation.ObjectAnimator;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.hardware.Camera;
import android.util.Log;
import android.view.Gravity;
import android.view.OrientationEventListener;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.atencjusz.projektkoncowybak.views.CameraPreview;
import com.atencjusz.projektkoncowybak.views.Circle;
import com.atencjusz.projektkoncowybak.helpers.ImageProcessing;
import com.atencjusz.projektkoncowybak.views.Miniature;
import com.atencjusz.projektkoncowybak.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CameraActivity extends AppCompatActivity {

    private String selectedAlbumName;
    private byte[] photoData;
    private ArrayList<byte[]> photosData= new ArrayList<>() ;
    private ArrayList<Miniature> miniaturesList;
    private Circle circle;
    private int rotation = 0;

    private OrientationEventListener orientationEventListener;

    /**VIEWS**/

    private FrameLayout frameLayout,
    miniaturesContainer;

    TextView textViewInfo;


    private ImageView ivAccept,
            ivCancel,
            ivChangeFacing,
            ivCamera,
            ivFilters,
            ivExposure,
            ivWhiteBalance,
            ivResolution;

    //camera
    private Camera camera;
    private int cameraId = -1;
    private CameraPreview _cameraPreview;
    private FrameLayout _frameLayout;
    private Camera.Parameters cameraParameters;
    private Camera.PictureCallback cameraPictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            // store photo data in byte[]
            // use it later
            // saving picture after user confirm taken photo
            photoData = data;
            photosData.add(photoData);

            // refresh camera
            //camera.stopPreview();
            camera.startPreview();

            //MINIATURE
            int miniatureSize = miniaturesContainer.getWidth()/4;
            Miniature miniature = new Miniature(CameraActivity.this, CameraActivity.this.photoData, miniatureSize);
            miniaturesList.add(miniature);
            CameraActivity.this.rearrangeMiniatures();


            miniaturesContainer.addView(miniature);

        }
    };
    private List<String> whiteBalanceList;
    private List<Camera.Size> pictureSizeList;
    private List<String> colorEffectsList;
    private int minExposure;
    private int maxExposure;
    private int currentExposure;

    private void initCamera(){
        boolean cameraSupported = getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);

        if (!cameraSupported) {
            // no camera
        } else {
            // wykorzystanie danych zwróconych przez kolejną funkcję getCameraId
            cameraId = getCameraId();
            // cam supported
            if (cameraId < 0) {
                // no selfie camera
            } else if (cameraId >= 0) {
                camera = Camera.open(cameraId);
                cameraParameters = camera.getParameters();
                whiteBalanceList = cameraParameters.getSupportedWhiteBalance();
                pictureSizeList = cameraParameters.getSupportedPictureSizes();
                colorEffectsList = cameraParameters.getSupportedColorEffects();

                minExposure = cameraParameters.getMinExposureCompensation();
                maxExposure = cameraParameters.getMaxExposureCompensation();
                currentExposure = cameraParameters.getExposureCompensation();
            }

        }
    }

    private void initPreview(){
        _cameraPreview = new CameraPreview(CameraActivity.this, camera);
        _frameLayout = (FrameLayout) findViewById(R.id.frameLayoutCameraActivity);
        _frameLayout.addView(_cameraPreview);

    }

    private void initViewFields(){

        this.frameLayout = (FrameLayout) findViewById(R.id.frameLayoutCameraActivity);
        this.circle = new Circle(CameraActivity.this);
        this.miniaturesContainer = new FrameLayout(CameraActivity.this){
            @Override
            protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
                super.onLayout(changed, left, top, right, bottom);
                circle.setRadius(miniaturesContainer.getWidth()/3);
                circle.setCenterPosition(miniaturesContainer.getWidth()/2,miniaturesContainer.getHeight()/2);
            }
        };
        this.frameLayout.addView(this.miniaturesContainer);

        this.miniaturesContainer.addView(this.circle);

        this.textViewInfo = new TextView(CameraActivity.this);
        textViewInfo.setGravity(Gravity.CENTER);
        textViewInfo.setBackgroundColor(Color.BLACK);
        textViewInfo.setTextColor(Color.WHITE);
        textViewInfo.setAlpha(.81f);

        this.ivAccept = (ImageView)findViewById(R.id.cameraCheck);
        this.ivCancel = (ImageView)findViewById(R.id.cameraCancel);
        this.ivChangeFacing = (ImageView)findViewById(R.id.cameraFacing);
        this.ivCamera = (ImageView)findViewById(R.id.cameraTakePicture);
        this.ivFilters = (ImageView)findViewById(R.id.cameraFilters);
        this.ivExposure = (ImageView)findViewById(R.id.cameraExposure);
        this.ivWhiteBalance = (ImageView)findViewById(R.id.cameraWhiteBalance);
        this.ivResolution = (ImageView)findViewById(R.id.cameraResolution);
    }

    private void initOnClickEventListeners(){
        final LinearLayout llAccept = (LinearLayout) ivAccept.getParent(),
                llCancel = (LinearLayout) ivCancel.getParent(),
                llFacing = (LinearLayout) ivChangeFacing.getParent(),
                llCamera = (LinearLayout) ivCamera.getParent();


        //DISABLED DUE TO TESTING OTHER FEATURES
        /*ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llAccept.setVisibility(View.VISIBLE);
                llCancel.setVisibility(View.VISIBLE);
                llFacing.setVisibility(View.GONE);
                llCamera.setVisibility(View.GONE);
                camera.takePicture(null, null, cameraPictureCallback);

                ivAccept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
                        String date = dateFormat.format(new Date());

                        File photo = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"JaroslawBak");
                        photo = new File(photo, selectedAlbumName);
                        photo = new File(photo.getAbsolutePath()+"/"+ date +".jpg");

                        try {
                            FileOutputStream fs = new FileOutputStream(photo);
                            fs.write(photoData);
                            fs.close();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        llAccept.setVisibility(View.GONE);
                        llCancel.setVisibility(View.GONE);
                        llFacing.setVisibility(View.VISIBLE);
                        llCamera.setVisibility(View.VISIBLE);
                        camera.startPreview();

                    }
                });

                ivCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        llAccept.setVisibility(View.GONE);
                        llCancel.setVisibility(View.GONE);
                        llFacing.setVisibility(View.VISIBLE);
                        llCamera.setVisibility(View.VISIBLE);
                        camera.startPreview();
                    }
                });


            }
        });*/

        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                camera.takePicture(null, null, cameraPictureCallback);
            }
        });

        ivFilters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(CameraActivity.this);
                alertDialog.setTitle("Color effects");
                //cannot have setMessage
                String[] tmp = new String[colorEffectsList.size()];

                for (int i = 0, length = colorEffectsList.size(); i < length; i++){
                    tmp[i] = colorEffectsList.get(i);
                }

                final String[] options = tmp;
                alertDialog.setItems(options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int index) {
                        //display options[which]);
                        cameraParameters.setColorEffect(options[index]);
                        camera.setParameters(cameraParameters);
                    }
                });
                alertDialog.show();
            }
        });

        ivExposure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(CameraActivity.this);
                alertDialog.setTitle("Exposure");
                int tmpLength = maxExposure - minExposure;
                String[] tmp = new String[tmpLength];

                for (int i = 0; i < tmpLength; i++){
                    tmp[i] = (minExposure + i) + "";
                }

                final String[] options = tmp;
                alertDialog.setItems(options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int index) {;
                        int tmp = Integer.parseInt(options[index]);
                        cameraParameters.setExposureCompensation(tmp);
                        camera.setParameters(cameraParameters);
                    }
                });
                alertDialog.show();
            }
        });

        ivWhiteBalance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(CameraActivity.this);
                alertDialog.setTitle("White balance");
                String[] tmp = new String[whiteBalanceList.size()];
                int i = 0;
                for (String s : whiteBalanceList){
                    tmp[i] = s;
                    i++;
                }
                final String[] options = tmp;
                alertDialog.setItems(options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int index) {
                        cameraParameters.setWhiteBalance(options[index]);
                        camera.setParameters(cameraParameters);
                    }
                });
                alertDialog.show();
            }
        });

        ivResolution.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(CameraActivity.this);
                alertDialog.setTitle("Resolution");
                //cannot have setMessage
                String[] tmp = new String[pictureSizeList.size()];
                int i = 0;
                for (Camera.Size s : pictureSizeList){
                    tmp[i] = s.width + "x" + s.height;
                    i++;
                }
                final String[] options = tmp;
                alertDialog.setItems(options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int index) {
                        //display options[which]);
                        cameraParameters.setPictureSize(pictureSizeList.get(index).width,pictureSizeList.get(index).height);
                        camera.setParameters(cameraParameters);
                    }
                });
                alertDialog.show();
            }
        });
    }

    private void initOrientationEventListener(){
        this.orientationEventListener = new OrientationEventListener(CameraActivity.this) {
            @Override
            public void onOrientationChanged(int i) {
                // i zwraca kąt 0 - 360 stopni podczas obracania ekranem w osi Z
                // tutaj wykonaj animacje butonów i miniatur zdjęć
                int j;
                if(i >= 315 || i < 45) {
                    j = 0;
                }
                else if(i >= 45 && i < 135) {
                    j = 90;
                }
                else if(i >= 135 && i < 225) {
                    j = 180;
                }
                else {
                    j = 270;
                }

                if(rotation != j) {

                    rotation = j;
                    if(rotation == 0) {
                        rotateViewsTo(0);
                    }
                    else if(rotation == 90) {
                        rotateViewsTo(270);
                    }
                    else if(rotation == 180) {
                        rotateViewsTo(180);
                    }
                    else {
                        rotateViewsTo(90);
                    }
                }
            }
        };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        getSupportActionBar().hide();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initCamera();
        initPreview();
        initViewFields();
        initOnClickEventListeners();
        initOrientationEventListener();

        Bundle b = this.getIntent().getExtras();
        selectedAlbumName = (String)b.get("albumname");

        miniaturesList = new ArrayList<>();

    }
    @Override
    protected void onPause(){
        super.onPause();
        if (camera != null) {
            camera.stopPreview();
            //linijka nieudokumentowana w API, bez niej jest crash przy wznawiamiu kamery
            _cameraPreview.getHolder().removeCallback(_cameraPreview);
            camera.release();
            camera = null;
        }

        orientationEventListener.disable();
    }

    @Override
    protected void onResume(){
        super.onResume();

        if (camera == null) {
            initCamera();
            initPreview();
            camera.startPreview();
        }

        if (orientationEventListener.canDetectOrientation()) {
            // Log - listener działa
            orientationEventListener.enable();
        } else {
            // Log - listener nie działa
        }

        this.frameLayout.removeView(this.miniaturesContainer);
        this.frameLayout.addView(this.miniaturesContainer);
    }

    private int getCameraId(){
        int cid = 0;
        int camerasCount = Camera.getNumberOfCameras(); // when more than one camera

        for (int i = 0; i < camerasCount; i++) {
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            Camera.getCameraInfo(i, cameraInfo);

            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cid = i;
            }
	    /*
            if (cameraInfo.facing == CameraInfo.CAMERA_FACING_FRONT) {
                cid = i;
            }
	    */
        }

        return cid;
    }

    private void rearrangeMiniatures(){
        float centerY = miniaturesContainer.getHeight()/2;//i expect this is the y coordinate of center
        float centerX = miniaturesContainer.getWidth()/2;//i expect this is the x coordinate of center
        for ( int i = 0, length = miniaturesList.size(); i < length; i++){
            miniaturesList.get(i).setCenterPosition(
                    centerX - circle.getRadius()*Math.sin((double)(i*2*Math.PI/length)),
                    centerY - circle.getRadius()*Math.cos((double)(i*2*Math.PI/length))
            );
            miniaturesList.get(i).invalidate();
        }
    }

    private void rotateViewsTo(int rotation) {

        rotateView(ivAccept, rotation);
        rotateView(ivCancel, rotation);
        rotateView(ivCamera, rotation);
        rotateView(ivChangeFacing, rotation);
        rotateView(ivFilters, rotation);
        rotateView(ivExposure, rotation);
        rotateView(ivWhiteBalance, rotation);
        rotateView(ivResolution, rotation);
        rotateView(miniaturesContainer,rotation);

        /*for ( int j = 0, length = miniaturesList.size(); j < length; j++){
            rotateView(miniaturesList.get(j),i);
        }*/

    }

    private void rotateView(View view, int i) {

        ObjectAnimator.ofFloat(view, View.ROTATION, view.getRotation(), i)
                .setDuration(750)
                .start();
    }

    public void showPhotoPreview(Bitmap bitmap){

        final ImageView iv = new ImageView(CameraActivity.this);
        iv.setImageBitmap(bitmap);
        iv.setRotation(90f);


        final FrameLayout fl = new FrameLayout(CameraActivity.this);
        /*{
            @Override
            protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
                super.onLayout(changed, left, top, right, bottom);
                double ratio = .8;
                iv.setLayoutParams(new LinearLayout.LayoutParams(
                        (int)(ratio*this.getWidth()),
                        (int)(ratio*this.getHeight()
                        )));
            }
        };*/
        fl.setBackgroundColor(Color.BLACK);
        fl.addView(iv);
        fl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                miniaturesContainer.removeView(fl);
            }
        });

        miniaturesContainer.addView(fl);
    }

    public void showInfo(String message){
        textViewInfo.setText(message);
        this.miniaturesContainer.addView(textViewInfo);
    }

    public void hideInfo(){
        textViewInfo.setText("");
        this.miniaturesContainer.removeView(textViewInfo);
    }

    public void deleteMiniature(Miniature miniature){
        miniaturesContainer.removeView(miniature);
        photosData.remove(miniaturesList.indexOf(miniature));
        miniaturesList.remove(miniature);
        this.rearrangeMiniatures();
    }

    public void deleteAllMiniatures(){
        for ( int i = 0, length = miniaturesList.size(); i < length; i++){
            miniaturesContainer.removeView(miniaturesList.get(i));
        }
        miniaturesList.clear();
        photosData.clear();
    }

    public void saveImage(Miniature miniature){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String date = dateFormat.format(new Date());

        File photo = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"JaroslawBak");
        photo = new File(photo, selectedAlbumName);
        photo = new File(photo.getAbsolutePath()+File.separatorChar+ date +".jpg");

        try {
            FileOutputStream fs = new FileOutputStream(photo);
            Bitmap bitmap = ImageProcessing.createBitmap(photosData.get(miniaturesList.indexOf(miniature)));
            int quality = 30;
            bitmap = ImageProcessing.rotateBitmap(bitmap);
            ImageProcessing.saveBitmapToJPEG(fs, bitmap, quality);
            fs.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.deleteMiniature(miniature);
    }

    public void saveAllImages(){
        Log.d("PDATALEN", Integer.toString(photosData.size()));
        for (int i = 0, length = photosData.size(); i < length; i++){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
            String date = dateFormat.format(new Date());

            File photo = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"JaroslawBak");
            photo = new File(photo, selectedAlbumName);
            photo = new File(photo.getAbsolutePath()+File.separatorChar+ date +".jpg");

            try {
                FileOutputStream fs = new FileOutputStream(photo);
                Bitmap bitmap = ImageProcessing.createBitmap(photosData.get(i));
                int quality = 30;
                bitmap = ImageProcessing.rotateBitmap(bitmap);
                ImageProcessing.saveBitmapToJPEG(fs, bitmap, quality);
                fs.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.deleteAllMiniatures();
    }
}
