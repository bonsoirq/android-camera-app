package com.atencjusz.projektkoncowybak.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.atencjusz.projektkoncowybak.views.ImageData;
import com.atencjusz.projektkoncowybak.helpers.ImageProcessing;
import com.atencjusz.projektkoncowybak.helpers.Prefs;
import com.atencjusz.projektkoncowybak.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CollageActivity extends AppCompatActivity {

    private ArrayList<ImageData> imageDatas;
    private ArrayList<ImageView> imageViews;
    private FrameLayout frameLayout;
    private ImageView currentImageView,
    ivFlip,
    ivRotate,
    ivSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collage);
        getSupportActionBar().hide();

        imageDatas = (ArrayList<ImageData>) getIntent().getSerializableExtra("list");
        this.frameLayout = (FrameLayout) findViewById(R.id.collage_frameLayout);
        this.imageViews = new ArrayList<>();
        ivFlip = (ImageView) findViewById(R.id.collage_flip);
        ivRotate = (ImageView) findViewById(R.id.collage_rotate);
        ivSave = (ImageView) findViewById(R.id.collage_save);

        for (int i = 0, length = imageDatas.size(); i < length; i++){
//            Log.d("datat", imageDatas.get(i).toString());
            ImageView imageView = new ImageView(CollageActivity.this);
            imageViews.add(imageView);
            imageView.setX(imageDatas.get(i).getX());
            imageView.setY(imageDatas.get(i).getY());
            imageView.setLayoutParams(new LinearLayout.LayoutParams(
                    ((int) imageDatas.get(i).getWidth()),
                    ((int) imageDatas.get(i).getHeight())
                )
            );
            imageView.setBackgroundColor(Color.WHITE);
            frameLayout.addView(imageView);
        }
        this.setListeners();
    }

    private void setListeners(){
        ivRotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Bitmap bitmap = ((BitmapDrawable) currentImageView.getDrawable()).getBitmap();
                    bitmap = ImageProcessing.rotateBitmap(bitmap);
                    currentImageView.setImageBitmap(bitmap);
                } catch (NullPointerException e){
                    e.printStackTrace();
                }
            }
        });

        ivFlip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Bitmap bitmap = ((BitmapDrawable) currentImageView.getDrawable()).getBitmap();
                    bitmap = ImageProcessing.flipBitmap(bitmap);
                    currentImageView.setImageBitmap(bitmap);
                } catch (NullPointerException e){
                    e.printStackTrace();
                }
            }
        });

        ivSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    frameLayout.setDrawingCacheEnabled(true);
                    Bitmap bitmap = frameLayout.getDrawingCache(true);
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
                    String date = dateFormat.format(new Date());

                    File photo = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"JaroslawBak");
                    photo = new File(photo, "kolaze");
                    if (!photo.exists()) photo.mkdir();
                    photo = new File(photo.getAbsolutePath()+File.separatorChar+ date +".jpg");

                    try {
                        FileOutputStream fs = new FileOutputStream(photo);
                        int quality = 30;
                        ImageProcessing.saveBitmapToJPEG(fs, bitmap, quality);
                        fs.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    CollageActivity.this.finish();
                } catch (NullPointerException e){
                    e.printStackTrace();
                }
            }
        });



        for (int i = 0, length = imageViews.size(); i < length; i++){
            final ImageView imageView = imageViews.get(i);
            imageView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    currentImageView = imageView;
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(CollageActivity.this);
                    alertDialog.setTitle("Wybierz źródło importu");
                    //cannot have setMessage

                    final String[] options = {
                            "Systemowy aparat",
                            "Wbudowany aparat",
                            "Galeria zdjęć"
                    };

                    alertDialog.setItems(options, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int index) {
                            //display options[which]);
                            Intent intent;
                            switch (index){
                                case 0:
                                    intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    //if camera is avaiable
                                    if (intent.resolveActivity(getPackageManager()) != null) {
                                        startActivityForResult(intent, 0x0A); // 0x0A <- result
                                    }
                                    break;
                                case 1:
                                    intent = new Intent(CollageActivity.this, CollageCameraActivity.class);
                                    startActivityForResult(intent, 0x0B);
                                    break;
                                case 2:
                                    intent = new Intent(Intent.ACTION_PICK);
                                    intent.setType("image/*");
                                    startActivityForResult(intent, 0x0C);
                                    break;
                                default:
                            }
                        }
                    });
                    alertDialog.show();
                    return false;
                }
            });
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    currentImageView = imageView;
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bitmap;
        Bundle extras;
        switch (requestCode){
            case 0x0A:
                extras = data.getExtras();
                bitmap = (Bitmap) extras.get("data");
                currentImageView.setImageBitmap(bitmap);
                break;
            case 0x0B:
//                extras = data.getExtras();
//                byte[] xdata = (byte[]) extras.get("photodata");
                bitmap = ImageProcessing.createBitmap(Prefs.getPhotoData());
                bitmap = ImageProcessing.rotateBitmap(bitmap);
                currentImageView.setImageBitmap(bitmap);
                break;
            case 0x0C:
                Uri imgData = data.getData();
                InputStream stream = null;
                try {
                    stream = getContentResolver().openInputStream(imgData);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                bitmap = BitmapFactory.decodeStream(stream);
                currentImageView.setImageBitmap(bitmap);
            default:
        }
        currentImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
    }
}
