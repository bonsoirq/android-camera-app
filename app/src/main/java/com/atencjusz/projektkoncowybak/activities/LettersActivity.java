package com.atencjusz.projektkoncowybak.activities;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.atencjusz.projektkoncowybak.R;
import com.atencjusz.projektkoncowybak.helpers.Prefs;
import com.atencjusz.projektkoncowybak.views.PreviewText;

import java.io.IOException;

public class LettersActivity extends AppCompatActivity {

    private int strokeColor = Color.RED, fillColor = Color.BLUE;
    private LinearLayout textContainer;
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_letters);
        getSupportActionBar().hide();

        Prefs.setSTROKE(Color.RED);
        Prefs.setFILL(Color.BLUE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);

        final LinearLayout scrollLayout = (LinearLayout) findViewById(R.id.letters_list);
        textContainer = (LinearLayout) findViewById(R.id.letters_container_textView);
        //final TextView textPreview = (TextView) findViewById(R.id.letters_textView);
        editText = (EditText)  findViewById(R.id.letters_editText);
        editText.clearFocus();
        final PreviewText previewText = new PreviewText(
                LettersActivity.this,
                editText.getText().toString(),
                Prefs.getPreviewTypeface()
        );
        textContainer.addView(previewText);
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //textPreview.setText(editText.getText());
                textContainer.removeAllViews();
                final PreviewText previewText = new PreviewText(
                        LettersActivity.this,
                        editText.getText().toString(),
                        Prefs.getPreviewTypeface()
                );
                textContainer.addView(previewText);
                previewText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        Prefs.setPreviewText(previewText.getText());
                        setResult(0x0F, intent);
                        finish();
                    }
                });
            }
        };
        editText.addTextChangedListener(textWatcher);

        AssetManager assetManager = getAssets();
        final String[] list;
        try {
            list = assetManager.list("fonts");
            for (int i = 0; i < list.length; i++){
                TextView textView = new TextView(LettersActivity.this);
                textView.setText(list[i]);
                textView.setTextSize(32f);
                scrollLayout.addView(textView);
                final int index = i;
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Typeface tf= Typeface.createFromAsset(getAssets(),"fonts/" + list[index]);
//                        Log.d("tf", tf.toString());
                        //textPreview.setTypeface (tf);
                        Prefs.setPreviewTypeface(tf);
                        //previewText.setTypeFace(tf);
                        //previewText.reset();
                        textContainer.removeAllViews();
                        final PreviewText previewText = new PreviewText(
                                LettersActivity.this,
                                editText.getText().toString(),
                                Prefs.getPreviewTypeface(),
                                Prefs.getSTROKE(),
                                Prefs.getFILL()
                        );
                        previewText.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent();
                                Prefs.setPreviewText(previewText.getText());
                                setResult(0x0F, intent);
                                finish();
                            }
                        });
                        textContainer.addView(previewText);
                    }
                });

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        /*textPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                Prefs.setPreviewText(textPreview.getText().toString());
                setResult(0x0F, intent);
                finish();
            }
        });*/

        previewText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                Prefs.setPreviewText(previewText.getText());
                setResult(0x0F, intent);
                finish();
            }
        });

        ImageView strokeColor = (ImageView) findViewById(R.id.strokeColor),
                fillColor = (ImageView) findViewById(R.id.fillColor);

        strokeColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LettersActivity.this, ColorPickerActivity.class);
                intent.putExtra("req", 0x0A);
                startActivityForResult(intent, 0x0A);
            }
        });

        fillColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LettersActivity.this, ColorPickerActivity.class);
                intent.putExtra("req", 0x0B);
                startActivityForResult(intent, 0x0B);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //int color = data.getIntExtra("color", 0);
        int color = Prefs.getCOLOR();
        switch (resultCode){
            case 0x0B:
                strokeColor = color;
                Prefs.setSTROKE(color);
                break;
            case 0x0A:
                fillColor = color;
                Prefs.setFILL(color);
                break;
        }
        textContainer.removeAllViews();
        final PreviewText previewText = new PreviewText(
                LettersActivity.this,
                editText.getText().toString(),
                Prefs.getPreviewTypeface(),
                strokeColor,
                fillColor
        );
        previewText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                Prefs.setPreviewText(previewText.getText());
                setResult(0x0F, intent);
                finish();
            }
        });
        textContainer.addView(previewText);
    }
}
