package com.atencjusz.projektkoncowybak.activities;

import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.atencjusz.projektkoncowybak.adapters.MyArrayAdapter;
import com.atencjusz.projektkoncowybak.helpers.Prefs;
import com.atencjusz.projektkoncowybak.R;

import java.io.File;
import java.util.ArrayList;

public class ChooseAlbumActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_album);
        getSupportActionBar().hide();

        //get GridView from XML
        GridView gridView = (GridView)findViewById(R.id.gridViewChooseAlbum);


        /*
         * add all directories to gridView
         */

        //store subdirectories in ArrayList
        final ArrayList<String> albumsArrList = new ArrayList<>();
        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"JaroslawBak");
        for (File f : dir.listFiles()){
            if (f.isDirectory())
                albumsArrList.add(f.getName());
        }

        String[] array = new String[albumsArrList.size()];
        for (int i = 0; i < albumsArrList.size(); i++){
            array[i] = albumsArrList.get(i);
        }
        //initialize array adapter for GridView
        MyArrayAdapter arrAdapter = new MyArrayAdapter(
                ChooseAlbumActivity.this,     // Context
                R.layout.layout_choose_album,     // nazwa pliku xml naszej komórki
                R.id.textView_layout_choose_album,         // id pola txt w komórce
                array);         // tablica przechowująca dane

        //append array adapter to view
        gridView.setAdapter(arrAdapter);

        //handle click event on gridview cell
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(ChooseAlbumActivity.this, CameraActivity.class);
                Prefs.setSelectedAlbum(albumsArrList.get(i));
                Bundle b = new Bundle();
                b.putString("albumname", albumsArrList.get(i));
                intent.putExtras(b);
                startActivity(intent);
            }
        });

    }

    public void restart(){
        //get GridView from XML
        GridView gridView = (GridView)findViewById(R.id.gridViewChooseAlbum);


        /*
         * add all directories to gridView
         */

        //store subdirectories in ArrayList
        ArrayList<String> albumsArrList = new ArrayList<>();
        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"JaroslawBak");
        for (File f : dir.listFiles()){
            if (f.isDirectory())
                albumsArrList.add(f.getName());
        }

        String[] array = new String[albumsArrList.size()];
        for (int i = 0; i < albumsArrList.size(); i++){
            array[i] = albumsArrList.get(i);
        }
        //initialize array adapter for GridView
        MyArrayAdapter arrAdapter = new MyArrayAdapter(
                ChooseAlbumActivity.this,     // Context
                R.layout.layout_choose_album,     // nazwa pliku xml naszej komórki
                R.id.textView_layout_choose_album,         // id pola txt w komórce
                array);         // tablica przechowująca dane

        //append array adapter to view
        gridView.setAdapter(arrAdapter);

        //handle click event on gridview cell
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //test
            }
        });
    }
}
