package com.atencjusz.projektkoncowybak.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.atencjusz.projektkoncowybak.adapters.ListArrayAdapter;
import com.atencjusz.projektkoncowybak.helpers.Prefs;
import com.atencjusz.projektkoncowybak.R;
import com.atencjusz.projektkoncowybak.views.PreviewText;

import java.io.ByteArrayOutputStream;

public class AlbumPhotoActivity extends AppCompatActivity {

    private ImageView ivFonts;
    private RelativeLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_photo);
        getSupportActionBar().hide();

        ivFonts = (ImageView) findViewById(R.id.preview_font);

        ivFonts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AlbumPhotoActivity.this, LettersActivity.class);
                startActivityForResult(intent, 0x0F);
//                startActivity(intent);
            }
        });

        container = (RelativeLayout) findViewById(R.id.preview_container);
        ImageView imageView = (ImageView) findViewById(R.id.albums_preview);
        imageView.setImageBitmap(Prefs.getPreviewPhoto());
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
//        imageView.setVisibility(View.GONE);

        String[] array = {
                "fonts",
                "upload",
                "share",
                "something"
        };

        ListView listView = (ListView) findViewById(R.id.preview_listView);
        ListArrayAdapter listArrayAdapter = new ListArrayAdapter(
                AlbumPhotoActivity.this,
                R.layout.layout_drawer,
                array
        );
        listView.setAdapter(listArrayAdapter);
    }

    public byte[] getPhotoData(){
        Bitmap bitmap = Prefs.getPreviewPhoto();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
        byte[] array = byteArrayOutputStream.toByteArray();
        return array;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0x0F){
            PreviewText previewText = new PreviewText(
                    AlbumPhotoActivity.this,
                    Prefs.getPreviewText(),
                    Prefs.getPreviewTypeface(),
                    Prefs.getSTROKE(),
                    Prefs.getFILL()
            );
            previewText.setLayoutParams(
                    new RelativeLayout.LayoutParams(
                            Math.abs(previewText.getRect().width() + 20),
                            Math.abs(previewText.getRect().height() + 40)
                    )
            );
            previewText.setX(300f);
            previewText.setY(300f);

            /*previewText.offX = 300;
            previewText.offY = 300;
*/
            previewText.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    PreviewText pt = (PreviewText) view;
                    switch (motionEvent.getAction()) {

                        case MotionEvent.ACTION_DOWN:
                            view.bringToFront();
                            pt.offX = view.getX() - motionEvent.getRawX();
                            pt.offY = view.getY() - motionEvent.getRawY();
                            break;

                        case MotionEvent.ACTION_MOVE:

                            view.animate()
                                    .x(motionEvent.getRawX() + pt.offX)
                                    .y(motionEvent.getRawY() + pt.offY)
                                    .setDuration(0)
                                    .start();
                            break;
                        default:
                            return false;
                    }
                    return true;
                }
            });
            Log.d("preview", previewText.toString());
            container.addView(previewText);
        }
    }
}
