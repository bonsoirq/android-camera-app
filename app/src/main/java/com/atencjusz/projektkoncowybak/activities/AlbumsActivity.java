package com.atencjusz.projektkoncowybak.activities;

import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;

import com.atencjusz.projektkoncowybak.R;

import java.io.File;
import java.util.ArrayList;

public class AlbumsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_albums);
        getSupportActionBar().hide();


        //get GridView from XML
        GridView gridView = (GridView)findViewById(R.id.gridViewAlbums);


        /*
         * add all directories to gridView
         */

        //store subdirectories in ArrayList
        final ArrayList<String> albumsArrList = new ArrayList<>();
        final File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"JaroslawBak");
        for (File f : dir.listFiles()){
            if (f.isDirectory())
                albumsArrList.add(f.getName());
        }

        //initialize array adapter for GridView
        ArrayAdapter<String> arrAdapter = new ArrayAdapter<>(
                AlbumsActivity.this,     // Context
                R.layout.layout_albums,     // nazwa pliku xml naszej komórki
                R.id.gridViewAlbumsText,         // id pola txt w komórce
                albumsArrList );         // tablica przechowująca dane

        //append array adapter to view
        gridView.setAdapter(arrAdapter);

        //handle click event on gridview cell
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //open clicked directory and store all filenames in array
                String albumName = albumsArrList.get(i);
                File album = new File(dir, albumName);
                ArrayList<String> filenames = new ArrayList<String>();
                for (File f : album.listFiles()){
                    if (f.isFile()){
                        filenames.add(f.getName());
                    }
                }
                Bundle bundle = new Bundle();
                bundle.putStringArrayList("filenames", filenames);
                bundle.putString("albumpath", album.getAbsolutePath() + "/");
                Intent intent = new Intent(AlbumsActivity.this,ImagesScrollViewActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);


            }
        });
    }
}
